# README #

This project is the start of my efforts to build a nice web-app installer application. I have seen this kind of setup already from some projects, but I want to try, and build my own.  
Maybe I can build something nice that can be included in other web-apps at some point.

### What is this repository for? ###

* Configure your required packages and get an overview of what is installed and what is missing

### Features ###

* check for required module dependencies (e.g. imagick, pgsql, ...)
* check if the php version is the minimum required or higher
* specify required modules or versions of modules (if applicable)

### How do I get set up? ###

* not ready yet, nothing to set up :-)

### Contribution guidelines ###

* Code review

### Who do I talk to? ###

* Repo owner or admin

## TODO ##

* implement ability to define required modules and versions and generate compiled output of the current status
* maybe pack all of that into a class? structure still not entirely clear
* see how I use a (self defined?) framework to display the contents instead of `var_dump`ing them