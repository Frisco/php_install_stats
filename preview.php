<?php require_once 'index.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <style>
            table {
                border-collapse : collapse;
                margin-bottom   : .5rem;
            }

            table th,
            table td {
                border  : 1px solid #333;
                padding : .2rem;
            }

            table td:first-child {
                min-width : 350px;
            }

            table td:nth-child(2) {
                min-width : 100px;
            }
        </style>
        <title>Preview</title>
    </head>
    <body>
        <?php foreach ( $allInfos as $label => $item ): ?>
            <table>
                <thead>
                    <tr>
                        <th colspan="2"><?php echo $label ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ( $item as $key => $value ): ?>
                        <tr>
                            <td><?php echo $key; ?></td>
                            <td><?php echo $value; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endforeach; ?>
    </body>
</html>
