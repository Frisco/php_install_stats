<?php


/* the current setup is an interesting collection but it does not really help me with what I want to do.
 * I want to separate the modules into "required" and "optional/recommended" and then have displayed if they are installed or not
 *
 * On top of that, I also want to have a php version comparison somehow.
 *
 * what is missing is the ability to define what is required and then actually query the existing resources and gather stuff.
 * currently I'm just gathering stuff that I said is required, but it's all a bit weired.
 * Why is the db stuff separate? does it have to be?
 * if it is not, how can I filter these things nicely?
 *
 * Is this even the best way to store all the information?
 * Maybe I should really create a nice object that just stores all of these things and has some functions to retrieve the things.
 *
 * the constructor should get the required and optional things, but how? php version, req_modules, opt_modules ?
 *
 * I think the gdInfo and versions of mysql and imagick are useless for the installer feature, they might be interesting for debugging.
*/


# Array to hold all the arrays with the specific values
$allInfos                       = [];
$allInfos[ 'php' ]              = getPHPInfo();
$allInfos[ 'webserver' ]        = getWebserverInfo();
$allInfos[ 'webserverModules' ] = getWebserverModules( [ 'mod_rewrite' ] );
$allInfos[ 'phpModules' ]       = getPHPModules( [ 'mbstring', 'openssl', 'gd', 'curl', 'json', 'yaml', 'imagick' ] );
$allInfos[ 'dbModules' ]        = getDBModules( [ 'mysqli', 'mysqlnd', 'pgsql', 'pdo', 'pdo_mysql', 'pdo_pgsql' ] );
$allInfos[ 'mysqliInfo' ]       = getMysqliInfo();
$allInfos[ 'gdInfo' ]           = getGDInfo();
$allInfos[ 'imagickInfo' ]      = getImagickInfo();

//var_dump( $allInfos );

#var_dump(Imagick::queryFonts());
#var_dump(Imagick::queryFormats());

function getPHPInfo(): array {
    
    $php              = [];
    $php[ 'version' ] = explode( '-', PHP_VERSION )[ 0 ];
    
    return $php;
}

function getWebserverInfo(): array {
    
    # Webserver info (apache, nginx or php dev server?)
    $webserverInfo = [];
    
    if ( apache_get_version() ) {
        $parts                        = explode( '/', explode( ' ', apache_get_version() )[ 0 ] );
        $webserverInfo[ 'webserver' ] = $parts[ 0 ];
        $webserverInfo[ 'version' ]   = $parts[ 1 ];
    }
    else {
        $webserverInfo[ 'webserver' ] = 'Currently unknown webserver.';
    }
    
    return $webserverInfo;
}

function getExtensions( $requiredExtensions = [], $moduleArray = [] ): array {
    
    $foundExtensions = [];
    foreach ( $requiredExtensions as $extension ) {
        $foundExtensions[ strtolower( $extension ) ] = 'No';
    }
    
    foreach ( $moduleArray as $extension ) {
        if ( in_array( strtolower( $extension ), $requiredExtensions, true ) ) {
            $foundExtensions[ strtolower( $extension ) ] = 'Yes';
        }
    }
    
    ksort( $foundExtensions );
    
    return $foundExtensions;
}

function getPHPModules( $requiredModules = [] ): array {
    
    return getExtensions( $requiredModules, get_loaded_extensions() );
}

function getWebserverModules( $requiredModules = [] ): array {
    
    return getExtensions( $requiredModules, apache_get_modules() );
}

# not sure if this has to be a separate function, but it might make it easier to display
function getDBModules( $requiredModules = [] ): array {
    
    return getExtensions( $requiredModules, get_loaded_extensions() );
}

function getMysqliInfo(): array {
    
    if ( in_array( 'mysqli', get_loaded_extensions(), true ) ) {
        
        return [ 'version' => explode( ' ', mysqli_get_client_info() )[ 1 ] ];
    }
    
    return [ 'version' => 'mysqli not installed or loaded!' ];
}

function getGDInfo(): array {
    
    $gdInfo = [];
    
    foreach ( gd_info() as $key => $value ) {
        if ( $value === true ) {
            $value = 'Yes';
        }
        elseif ( $value === false ) {
            $value = 'No';
        }
        $gdInfo[ $key ] = $value;
    }
    
    return $gdInfo;
}

function getImagickInfo(): array {
    
    if ( in_array( 'imagick', get_loaded_extensions(), true ) ) {
        $version = Imagick::getVersion();
        preg_match( '/ImageMagick ([0-9]+\.[0-9]+\.[0-9]+)/', $version[ 'versionString' ], $version );
        
        return [ 'version' => $version[ 1 ] ];
    }
    
    return [ 'version' => 'Imagick not installed or loaded!' ];
}

# function with side-effects!!!
function serverDebugOutput(): string {
    
    $indicesServer = [ 'PHP_SELF',
                       'argv',
                       'argc',
                       'GATEWAY_INTERFACE',
                       'SERVER_ADDR',
                       'SERVER_NAME',
                       'SERVER_SOFTWARE',
                       'SERVER_PROTOCOL',
                       'REQUEST_METHOD',
                       'REQUEST_TIME',
                       'REQUEST_TIME_FLOAT',
                       'QUERY_STRING',
                       'DOCUMENT_ROOT',
                       'HTTP_ACCEPT',
                       'HTTP_ACCEPT_CHARSET',
                       'HTTP_ACCEPT_ENCODING',
                       'HTTP_ACCEPT_LANGUAGE',
                       'HTTP_CONNECTION',
                       'HTTP_HOST',
                       'HTTP_REFERER',
                       'HTTP_USER_AGENT',
                       'HTTPS',
                       'REMOTE_ADDR',
                       'REMOTE_HOST',
                       'REMOTE_PORT',
                       'REMOTE_USER',
                       'REDIRECT_REMOTE_USER',
                       'SCRIPT_FILENAME',
                       'SERVER_ADMIN',
                       'SERVER_PORT',
                       'SERVER_SIGNATURE',
                       'PATH_TRANSLATED',
                       'SCRIPT_NAME',
                       'REQUEST_URI',
                       'PHP_AUTH_DIGEST',
                       'PHP_AUTH_USER',
                       'PHP_AUTH_PW',
                       'AUTH_TYPE',
                       'PATH_INFO',
                       'ORIG_PATH_INFO' ];
    
    echo '<table border="0" cellpadding="2" cellspacing="2">';
    foreach ( $indicesServer as $arg ) {
        if ( isset( $_SERVER[ $arg ] ) ) {
            echo '<tr><td>' . $arg . '</td><td>' . $_SERVER[ $arg ] . '</td></tr>';
        }
        else {
            echo '<tr><td>' . $arg . '</td><td>-</td></tr>';
        }
    }
    echo '</table>';
    
}

?>
